#!/bin/bash
set -e

if [ "$ENV" = 'DEV' ]; then
  echo "Running Development Server"
  exec python /app/identity.py
elif [ "$ENV" = 'UNIT' ]; then
  echo "Running unit tests"
  exec python /app/tests.py
else
  echo "Running in Production mode"
  exec uwsgi --http 0.0.0.0:9090 --wsgi-file /app/identity.py --callable app --stats 0.0.0.0:9191
fi
